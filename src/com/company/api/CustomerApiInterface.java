package com.company.api;

import com.company.dto.AccountApplication;
import com.company.model.Customer;

public interface CustomerApiInterface {
//    public void createCustomerAccount(IndividualCustomer customer, CustomerAgreement agreement);
    public void createCustomerAccount(AccountApplication accountApplication);
//    public IndividualCustomer getCustomer(String pesel);
    Customer getCustomer(String pesel);
}
