// trener: Tomasz Kucharski

package com.company;

import com.company.model.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Address mainAddress = new Address(
                AddressType.MAIN_ADDRESS,
                "High Street",
                "NY",
                "USA",
                "332");

        List<Contact> contacts = new ArrayList<>();
        contacts.add(
                new Contact(ContactType.EMAIL,"1" ));


//        CustomerProduct secondCustomer = new Company();


        // individual
        IndividualCustomer individualCustomer = new IndividualCustomer(
                "Ann",
                "Kowalsky",
                mainAddress,
                contacts);
        System.out.println(individualCustomer);

        Product cycle = new Product("Any product", new Double(1000));
        System.out.println(cycle);

        individualCustomer.addProduct(cycle);
        System.out.println(individualCustomer);

        individualCustomer.removeProduct(cycle.toString());
        individualCustomer.addBonus("fuel discount");



    }

    private static void foo(CustomerProductUsage productUsge, Product product) {
        productUsge.addProduct(product);
    }


}
