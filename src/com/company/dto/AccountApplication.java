package com.company.dto;

import com.company.model.Address;
import com.company.model.Contact;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class AccountApplication {
    LocalDate agreementStartDate;
    LocalDate startDate;
    UUID customerID;
    String surname;
    Address address;
    List<Contact> contactList;


    public AccountApplication(LocalDate agreementStartDate, LocalDate startDate, UUID customerID, String surname, Address address, List<Contact> contactList) {
        this.agreementStartDate = agreementStartDate;
        this.startDate = startDate;
        this.customerID = customerID;
        this.surname = surname;
        this.address = address;
        this.contactList = contactList;
    }
}
