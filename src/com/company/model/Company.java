package com.company.model;

public class Company extends Customer implements CustomerProductUsage{
    String nip;

    public Company(String description, String nip) {
        super(description);
        this.nip = nip;
    }

    public Company(String nip) {
        this.nip = nip;
    }

    public Company() {
    }

    @Override
    public void addProduct(Product product) {

    }

    @Override
    public void removeProduct(String name) {

    }
}
