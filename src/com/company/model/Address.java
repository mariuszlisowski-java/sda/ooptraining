package com.company.model;

public class Address {
    AddressType addressType;
    String street;
    String city;
    String country;
    String houseNo;

    public Address(AddressType addressType, String street, String city, String country, String houseNo) {
        this.addressType = addressType;
        this.street = street;
        this.city = city;
        this.country = country;
        this.houseNo = houseNo;
    }

    @Override
    public String toString() {
        return "Address{" +
                "addressType=" + addressType +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", houseNo='" + houseNo + '\'' +
                '}';
    }
}
