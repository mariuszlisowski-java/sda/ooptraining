package com.company.model;

import java.util.ArrayList;
import java.util.List;

public class IndividualCustomer extends Customer implements CustomerProductUsage {
//    UUID customerID;
    String name;
    String surname;
    Address address;
    List<Contact> contactList;
    List<Product> productList;

    public IndividualCustomer(String name, String surname, Address address, List<Contact> contactList) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.contactList = contactList;
        this.productList = new ArrayList<>();
//        this.customerID = UUID.randomUUID();
    }

    @Override
    public void addProduct(Product product) {
        productList.add(product);
    }

    @Override
    public void removeProduct(String name) {
        for (Product product : new ArrayList<>(productList)) {
            if (name != null & name.equals(product.name)) {
                productList.remove(product);
            }
        }
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", address=" + address +
                ", contactList=" + contactList +
                ", productList=" + productList +
                '}';
    }
}

//        productList.removeIf(el -> el.name.equals(name));
