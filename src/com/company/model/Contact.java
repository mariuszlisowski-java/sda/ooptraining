package com.company.model;

public class Contact {
    ContactType contact;
    String value;

    public Contact(ContactType contact, String value) {
        this.contact = contact;
        this.value = value;
    }
}
