package com.company.model;

import java.util.UUID;

public abstract class Customer {
    UUID customerID;
    protected String bonus;
    protected String description;

    public Customer(String description) {
        this.customerID = UUID.randomUUID();
        this.description = description;
    }

    public void addBonus(String bonusName) {
        this.bonus = bonusName;
    }

    public Customer() {
    }
}
